﻿using HomeWork1.Models;
using Microsoft.EntityFrameworkCore;

namespace HomeWork1;

public partial class SberbankContext : DbContext
{
    public SberbankContext()
    {
    }

    public SberbankContext(DbContextOptions<SberbankContext> options)
        : base(options)
    {
    }

    public virtual DbSet<Account> Accounts { get; set; }

    public virtual DbSet<AccountType> AccountTypes { get; set; }

    public virtual DbSet<Client> Clients { get; set; }

    public virtual DbSet<Office> Offices { get; set; }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        optionsBuilder.UseNpgsql("Host=localhost;Port=5432;Database=sberbank;Username=postgres;Password=1234");
    }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<Account>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("account_pk");

            entity.ToTable("account");

            entity.Property(e => e.Id)
                .UseIdentityAlwaysColumn()
                .HasColumnName("id");
            entity.Property(e => e.DateClosed).HasColumnName("date_closed");
            entity.Property(e => e.DateCreate).HasColumnName("date_create");
            entity.Property(e => e.IdAccountType).HasColumnName("id_account_type");
            entity.Property(e => e.IdClient).HasColumnName("id_client");
            entity.Property(e => e.IdOffice).HasColumnName("id_office");
            entity.Property(e => e.Total).HasColumnName("total");

            entity.HasOne(d => d.IdAccountTypeNavigation).WithMany(p => p.Accounts)
                .HasForeignKey(d => d.IdAccountType)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("account_account_type_fk");

            entity.HasOne(d => d.IdClientNavigation).WithMany(p => p.Accounts)
                .HasForeignKey(d => d.IdClient)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("account_client_fk");

            entity.HasOne(d => d.IdOfficeNavigation).WithMany(p => p.Accounts)
                .HasForeignKey(d => d.IdOffice)
                .HasConstraintName("account_office_fk");
        });

        modelBuilder.Entity<AccountType>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("account_type_pk");

            entity.ToTable("account_type");

            entity.Property(e => e.Id)
                .UseIdentityAlwaysColumn()
                .HasColumnName("id");
            entity.Property(e => e.Name)
                .HasColumnType("character varying")
                .HasColumnName("name");
        });

        modelBuilder.Entity<Client>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("client_pk");

            entity.ToTable("client");

            entity.Property(e => e.Id)
                .UseIdentityAlwaysColumn()
                .HasColumnName("id");
            entity.Property(e => e.Address)
                .HasColumnType("character varying")
                .HasColumnName("address");
            entity.Property(e => e.Name)
                .HasColumnType("character varying")
                .HasColumnName("name");
            entity.Property(e => e.PassportNumber)
                .HasColumnType("character varying")
                .HasColumnName("passport_number");
            entity.Property(e => e.PassportSeries)
                .HasColumnType("character varying")
                .HasColumnName("passport_series");
            entity.Property(e => e.Phone)
                .HasColumnType("character varying")
                .HasColumnName("phone");
        });

        modelBuilder.Entity<Office>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("office_pk");

            entity.ToTable("office");

            entity.Property(e => e.Id)
                .UseIdentityAlwaysColumn()
                .HasColumnName("id");
            entity.Property(e => e.Address)
                .HasColumnType("character varying")
                .HasColumnName("address");
            entity.Property(e => e.Manager)
                .HasColumnType("character varying")
                .HasColumnName("manager");
            entity.Property(e => e.Phone)
                .HasColumnType("character varying")
                .HasColumnName("phone");
        });
        modelBuilder.HasSequence("account_id_seq").HasMax(2147483647L);
        modelBuilder.HasSequence("account_type_id_seq").HasMax(2147483647L);
        modelBuilder.HasSequence("client_id_seq").HasMax(2147483647L);
        modelBuilder.HasSequence("office_id_seq").HasMax(2147483647L);

        OnModelCreatingPartial(modelBuilder);
    }

    partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
}