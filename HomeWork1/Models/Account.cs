﻿namespace HomeWork1.Models;

public class Account
{
    public int Id { get; set; }

    public int IdClient { get; set; }

    public DateOnly DateCreate { get; set; }

    public DateOnly DateClosed { get; set; }

    public int IdAccountType { get; set; }

    public decimal Total { get; set; }

    public int? IdOffice { get; set; }

    public virtual AccountType IdAccountTypeNavigation { get; set; } = null!;

    public virtual Client IdClientNavigation { get; set; } = null!;

    public virtual Office? IdOfficeNavigation { get; set; }

    public override string ToString()
    {
        return
            $"{Id}.{IdClient}, дата оформления: {DateCreate}, дата закрытия: {DateClosed}, тип счета: {IdAccountType}, сумма: {Total} руб, адрес филиала: {IdOffice}";
    }
}