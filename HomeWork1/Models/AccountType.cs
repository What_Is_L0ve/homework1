﻿namespace HomeWork1.Models;

public class AccountType
{
    public int Id { get; set; }

    public string Name { get; set; } = null!;

    public virtual ICollection<Account> Accounts { get; } = new List<Account>();

    public override string ToString()
    {
        return $"{Id}.{Name}";
    }
}