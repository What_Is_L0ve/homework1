﻿namespace HomeWork1.Models;

public class Office
{
    public int Id { get; set; }

    public string Address { get; set; } = null!;

    public string Phone { get; set; } = null!;

    public string Manager { get; set; } = null!;

    public virtual ICollection<Account> Accounts { get; } = new List<Account>();

    public override string ToString()
    {
        return $"{Id}. Адрес: {Address}, телефон: {Phone}, менеджер: {Manager}";
    }
}