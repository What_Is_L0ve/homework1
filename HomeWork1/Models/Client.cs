﻿namespace HomeWork1.Models;

public class Client
{
    public int Id { get; set; }

    public string Name { get; set; } = null!;

    public string Phone { get; set; } = null!;

    public string PassportSeries { get; set; } = null!;

    public string PassportNumber { get; set; } = null!;

    public string Address { get; set; } = null!;

    public virtual ICollection<Account> Accounts { get; } = new List<Account>();

    public override string ToString()
    {
        return $"{Id}.{Name}, телефон: {Phone}, паспорт: {PassportSeries} {PassportNumber}, адрес: {Address}";
    }
}