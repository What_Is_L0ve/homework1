﻿using HomeWork1.Models;

namespace HomeWork1;

public class Program
{
    private static void Main(string[] args)
    {
        using var context = new SberbankContext();
        var canExit = false;

        while (!canExit)
        {
            Console.Write(
                "\n1.Отобразить все сущности" +
                "\n2.Добавить тип лицевого счета" +
                "\n3.Добавить клиента" +
                "\n4.Добавить лицевой счет" +
                "\n5.Добавить филиал" +
                "\n6.Завершить\n");
            if (int.TryParse(Console.ReadLine(), out var data))
                switch (data)
                {
                    case 1:
                        PrintResult(context);
                        break;
                    case 2:
                        AddAccountType(context);
                        break;
                    case 3:
                        AddClient(context);
                        break;
                    case 4:
                        AddAccount(context);
                        break;
                    case 5:
                        AddOffice(context);
                        break;
                    case 6:
                        canExit = true;
                        break;
                    default:
                        Console.WriteLine("Неверные данные");
                        break;
                }
        }
    }

    private static void PrintResult(SberbankContext context)
    {
        Console.WriteLine("\nClients list:");
        foreach (var client in context.Clients.ToList()) Console.WriteLine(client.ToString());

        Console.WriteLine("\nAccounts list:");
        foreach (var account in context.Accounts.ToList()) Console.WriteLine(account.ToString());

        Console.WriteLine("\nAccountTypes list:");
        foreach (var accountType in context.AccountTypes.ToList()) Console.WriteLine(accountType.ToString());

        Console.WriteLine("\nOffices list:");
        foreach (var office in context.Offices.ToList()) Console.WriteLine(office.ToString());
    }

    private static void AddOffice(SberbankContext context)
    {
        Console.WriteLine("Введите адрес филиала");
        var address = Console.ReadLine();

        Console.WriteLine("Введите номер телефона филиала");
        var phone = Console.ReadLine();

        Console.WriteLine("Введите ФИО менеджера филиала");
        var manager = Console.ReadLine();

        if (string.IsNullOrEmpty(address) || string.IsNullOrEmpty(phone) ||
            string.IsNullOrEmpty(manager)) return;

        var office = new Office
        {
            Id = 0, Address = address, Phone = phone, Manager = manager
        };
        context.Offices.Add(office);
        context.SaveChanges();
    }

    private static void AddClient(SberbankContext context)
    {
        Console.WriteLine("Введите ФИО клиента");
        var name = Console.ReadLine();

        Console.WriteLine("Введите номер телефона клиента");
        var phone = Console.ReadLine();

        Console.WriteLine("Введите серию паспорта клиента");
        var passportSeries = Console.ReadLine();

        Console.WriteLine("Введите номер паспорта клиента");
        var passportNumber = Console.ReadLine();

        Console.WriteLine("Введите адрес клиента");
        var address = Console.ReadLine();

        if (string.IsNullOrEmpty(name) || string.IsNullOrEmpty(phone) ||
            string.IsNullOrEmpty(passportSeries) || string.IsNullOrEmpty(passportNumber) ||
            string.IsNullOrEmpty(address)) return;

        var client = new Client
        {
            Id = 0, Name = name, Phone = phone, PassportSeries = passportSeries,
            PassportNumber = passportNumber, Address = address
        };
        context.Clients.Add(client);
        context.SaveChanges();
    }

    private static void AddAccountType(SberbankContext context)
    {
        Console.WriteLine("Введите наименование типа лицевого счета: ");
        var name = Console.ReadLine();

        if (string.IsNullOrEmpty(name)) return;

        var accountType = new AccountType { Id = 0, Name = name };
        context.AccountTypes.Add(accountType);
        context.SaveChanges();
    }

    private static void AddAccount(SberbankContext context)
    {
        Console.WriteLine("Введите ID клиента");
        int.TryParse(Console.ReadLine(), out var clientId);

        Console.WriteLine("Введите дату оформления счета");
        DateOnly.TryParse(Console.ReadLine(), out var dateCreate);

        Console.WriteLine("Введите дату закрытия счета");
        DateOnly.TryParse(Console.ReadLine(), out var dateClosed);

        Console.WriteLine("Введите ID типа счета");
        int.TryParse(Console.ReadLine(), out var accountTypeId);

        Console.WriteLine("Введите сумму средств");
        decimal.TryParse(Console.ReadLine(), out var total);

        Console.WriteLine("Введите ID филиала");
        int.TryParse(Console.ReadLine(), out var officeId);

        if (clientId == 0 || dateCreate == DateOnly.MinValue ||
            dateClosed == DateOnly.MinValue || accountTypeId == 0 ||
            total == 0 ||
            officeId == 0) return;

        var account = new Account
        {
            Id = 0,
            IdClient = clientId,
            DateCreate = dateCreate,
            DateClosed = dateClosed,
            IdAccountType = accountTypeId,
            Total = total,
            IdOffice = officeId
        };
        context.Accounts.Add(account);
        context.SaveChanges();
    }
}