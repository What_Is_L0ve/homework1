CREATE SCHEMA public AUTHORIZATION pg_database_owner;

CREATE SEQUENCE public.account_id_seq
	INCREMENT BY 1
	MINVALUE 1
	MAXVALUE 2147483647
	START 1
	CACHE 1
	NO CYCLE;

CREATE SEQUENCE public.account_type_id_seq
	INCREMENT BY 1
	MINVALUE 1
	MAXVALUE 2147483647
	START 1
	CACHE 1
	NO CYCLE;

CREATE SEQUENCE public.client_id_seq
	INCREMENT BY 1
	MINVALUE 1
	MAXVALUE 2147483647
	START 1
	CACHE 1
	NO CYCLE;

CREATE SEQUENCE public.office_id_seq
	INCREMENT BY 1
	MINVALUE 1
	MAXVALUE 2147483647
	START 1
	CACHE 1
	NO CYCLE;

CREATE TABLE public.account_type (
	id int4 NOT NULL GENERATED ALWAYS AS IDENTITY,
	"name" varchar NOT NULL,
	CONSTRAINT account_type_pk PRIMARY KEY (id)
);

CREATE TABLE public.client (
	id int4 NOT NULL GENERATED ALWAYS AS IDENTITY,
	"name" varchar NOT NULL,
	phone varchar NOT NULL,
	passport_series varchar NOT NULL,
	passport_number varchar NOT NULL,
	address varchar NOT NULL,
	CONSTRAINT client_pk PRIMARY KEY (id)
);

CREATE TABLE public.office (
	id int4 NOT NULL GENERATED ALWAYS AS IDENTITY,
	address varchar NOT NULL,
	phone varchar NOT NULL,
	manager varchar NOT NULL,
	CONSTRAINT office_pk PRIMARY KEY (id)
);

CREATE TABLE public.account (
	id int4 NOT NULL GENERATED ALWAYS AS IDENTITY,
	id_client int4 NOT NULL,
	date_create date NOT NULL,
	date_closed date NOT NULL,
	id_account_type int4 NOT NULL,
	total numeric NOT NULL,
	id_office int4 NOT NULL,
	CONSTRAINT account_pk PRIMARY KEY (id),
	CONSTRAINT account_account_type_fk FOREIGN KEY (id_account_type) REFERENCES public.account_type(id),
	CONSTRAINT account_client_fk FOREIGN KEY (id_client) REFERENCES public.client(id),
	CONSTRAINT account_office_fk FOREIGN KEY (id_office) REFERENCES public.office(id)
);


INSERT INTO public.account_type
("name")
VALUES('Ипотека');

INSERT INTO public.account_type
("name")
VALUES('Кредит');

INSERT INTO public.client
("name", phone, passport_series, passport_number, address)
VALUES('Березов А.В', '88888888888', '1111', '111111', 'ул.Ленина, 14');

INSERT INTO public.client
("name", phone, passport_series, passport_number, address)
VALUES('Иванов И.И', '77777777777', '2222', '222222', 'ул.Ленина, 15');

INSERT INTO public.client
("name", phone, passport_series, passport_number, address)
VALUES('Михайлов О.В', '66666666666', '3333', '333333', 'ул.Красина, 25');

INSERT INTO public.client
("name", phone, passport_series, passport_number, address)
VALUES('Каменев И.П', '55555555555', '4444', '444444', 'ул.Красина, 18');

INSERT INTO public.client
("name", phone, passport_series, passport_number, address)
VALUES('Травкин М.Р', '44444444444', '5555', '555555', 'ул.Гоголя, 1');

INSERT INTO public.office
(address, phone, manager)
VALUES('ул.Пушкина, 13/1', '333333', 'Осипов А.С');

INSERT INTO public.office
(address, phone, manager)
VALUES('ул.9 мая, 3', '444444', 'Радионов С.М');

INSERT INTO public.office
(address, phone, manager)
VALUES('ул.Красина, 40', '555555', 'Семенова И.Р');

INSERT INTO public.office
(address, phone, manager)
VALUES('ул.Первомайская, 10', '666666', 'Иванов А.М');

INSERT INTO public.office
(address, phone, manager)
VALUES('ул.Гоголя, 20а', '777777', 'Петров А.В');

INSERT INTO public.account
(id_client, date_create, date_closed, id_account_type, total, id_office)
VALUES(1, '21-02-2023', '21-02-2033', 1, 10000000, 1);

INSERT INTO public.account
(id_client, date_create, date_closed, id_account_type, total, id_office)
VALUES(2, '22-02-2023', '22-02-2030', 1, 5000000, 2);

INSERT INTO public.account
(id_client, date_create, date_closed, id_account_type, total, id_office)
VALUES(3, '23-02-2023', '23-02-2024', 2, 100000, 3);

INSERT INTO public.account
(id_client, date_create, date_closed, id_account_type, total, id_office)
VALUES(4, '24-02-2023', '24-08-2033', 2, 50000, 4);

INSERT INTO public.account
(id_client, date_create, date_closed, id_account_type, total, id_office)
VALUES(5, '25-02-2023', '25-02-2028', 1, 3000000, 5);